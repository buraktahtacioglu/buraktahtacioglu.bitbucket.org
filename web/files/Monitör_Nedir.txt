Monitör Nedir?

Monitör, görüntü sergilemek için kullanılan elektronik ya da elektro

-

mekanik aygıtların genel adıdır.

Monitör, başta televizyon ve bilgisayar olmak üzere birçok elektronik cihazın en önemli çıktı

aygıtıdır. Monitör, plastik bir muhafaza

içerisinde gerekli elektronik devreleri, güç transformatörünü

ve resmi oluşturan birimleri içerir. Monitörle bilgisayar arasındaki iletişimi ekran kartı sağlar. Yani,

monitörden çıkan veri kablosu bilgisayar kasasında ekran kartına bağlanır. Monitörlerin b

oyutları

inç ölçü cinsiyle ifade edilir. Bu boyut monitör ekranının bir köşesinden diğer köşesine olan

uzaklıktır.

Monitör Türleri

Monitörler iki tiptedir. CRT ve daha modern olan LCD monitörler. CRT monitörlerin boyutları bir

televizyon gibi, oldukça büy

üktür. LCD monitörler ise çok daha incedir.

CRT Monitörler

Bir monitörün en önemli parçası çeşitli elektronik devrelerle birlikte CRT (Chatode Ray Tube

–

Katot Işınlı Tüp) denilen havası boşaltılmış ve ön yüzeyi binlerce fosfor noktacığından (dot)

oluşan k

oni şeklindeki tüptür. Bu tüpün geniş tarafı dikdörtgen şeklindedir. Diğer dar tarafında ise

elektron tabancası bulunur. Tabanca içerisindeki katot levhaları tel fleman (ısıtıcı) ile ısıtılır ve tüp

içerisinde serbestçe dolaşan elektron bulutu oluşturulur.

Negatif kutuplandırılan katotlar ile pozitif

kutuplandırılan ekranın iç yüzeyi arasında büyük bir gerilim farkı uygulandığında katotlarda

oluşan elektronlar dış yüzeye doğru fırlar. Sabit olarak yerleştirilen odaklama elemanları bu

elektronları bir araya getirerek bir ışın halinde ekran orta yüzeyinde odaklar. Bu ışını ekranın

istenilen taraflarına yönlendirmek için elektron tabancasının etr

afında yatay ve dikey saptırma

bobinleri bulunur. İşte bu ışının ön yüzeyde gezdirilmesi suretiyle ortaya görüntüler çıkar. Ekran

kartından sinyal geldiği müddetçe bu ışında monitörün sol üst köşesinden başlayarak fosfor ile

kaplı ön yüzeyi tarar. Burada v

erecek çinko oksit türevi kullanılır. Noktanın hızlı hareketi

görüntüyü oluşturur. Gözümüz gecikmeli algıladığı için görüntü oluşur. Elektron demetinin

ekranda saniyede kaç resim taradığı ekran kartı tarafından belirlenir. Bu değer saniyede 50 ile

120 aras

ında değişir. Bu değerler “tazeleme” frekansı olarak isimlendirilir. Değerin yüksek olması

görüntü kalitesini ciddi ölçüde artıracaktır. Değer düşük olursa monitörde gözü yoran kıpraşımlar

daha da fazla olacaktır. Renkli monitörlerde renklerin oluşması içi

n üç temel renk (kırmızı

-

yeşil

-

mavi) kullanılır. Her renk için elektron tabancası içerisinde bir ışın demeti oluşturan eleman vardır.

Ayrıca ekran yüzeyi de üç ayrı renkten oluşan fosfor tabakasından oluşur. Bu tabakalar delikli bir

maskenin arasından aydı

nlatılır. Hassas bir şekilde ayarlanan bu deliklerde her renge ait ışın

demeti sadece o renge çarpar. Monitördeki her nokta üç ayrı renkteki fosfor damlacığından

oluşur. Bu üç fosfor damlacığı da bir araya gelerek “pixel” leri oluşturur. Birbirine en yakın

aynı

renkteki iki noktanın merkezleri arasındaki uzaklığa “dot pitch” denir. Nokta aralığı anlamına gelen

bu ifadenin bu günkü değerleri 0.24 mm ile 0.28 mm arasında değişmektedir. Bu değerlerin küçük

olması görüntü kalitesinin artması anlamına gelir.

LCD

Monitörler

Ana madde: Sıvı kristal ekran LCD (Liquid Cyristal Display) monitörlerde görüntü sıvı kristal

diyotlar yardımıyla sağlanmaktadır. Bu diyotlara gerilim uygulandığında, içlerindeki moleküllerin

polarizasyonu değişmekte ve beraberinde de diyotun g

eçirgenliği değişmektedir. Bu duruma

dijital saatlerde de rastlamaktayız. Normalde şeffaf olan bu diyotlara gerilim uygulandığında

geçirgenliklerini kaybederler ve siyaha dönerler. Renkli LCD monitörlerde ise çok ufak ve birden

fazla diyot kamanı kullanıla

rak görüntü alınmaktadır. LCD monitörler DSTN ve TFT olmak üzere

ikiye ayrılmaktadır. Ucuz olan ve “passive matrix” teknolojisini kullanan DSTN (Dual

-

Scan Twisted

Nematic)’ler çözünürlükleri ve görüş açıları TFT’lerden düşük olan monitörlerdir. Bu monitörl

er

genelde dizüstü bilgisayarlarda kullanılmaktadır. TFT (Thin Film Transistor)’ler ise “active matrix”




adı verilen ve görüntüyü daha parlak ve keskin gösteren bir teknoloji kullanırlar. TFT’lerde her

piksel bir ya da dört transistör tarafından kontrol edi

lir ve bu sayede flat panel ekranlar arasında

en iyi çözünürlüğü sunarlar..

Monitör Boyutları



15 inç

-

38 cm ekran



17 inç

-

43 "



19 inç

-

48 "



22 inç

-

56 "



24 inç

-

60 "



26 inç

-

66 "



32 inç

-

82 "



40 inç

-

102 "



42 inç

-

106 "



46 inç

-

116 "




