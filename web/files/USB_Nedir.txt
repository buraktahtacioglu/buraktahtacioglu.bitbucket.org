USB Nedir?

USB, İngilizce "Universal Serial Bus" kelimesinin kısaltmasıdır. USB'nin türkçesi "Evrensel Seri

Veriyolu"dur. USB dış donanımların bilgisayar ile bağlantı kurabilmesini sağlayan seri yapılı bir

bağlantı biçimidir. Son sürümü 3.0'dır. 600 MByte

/sn'lik aktarım hızı vardır. Standart bir USB 2.0

veriyolu 5.00 volt, 500 mA çıkış verirken USB 3.0 veriyolu 900 mA çıkış değerine sahiptir. Tak

Çalıştır (plug and play, PnP) özelliğinden dolayı birçok cihazın bağlantısında kullanılmaktadır.

Evrensel seri

veriyolu, çevre birimlerinin bilgisayara takıldıkları anda tanınıp otomatik çalışmalarını

sağlamaktadır. Yani PnP'dir. Bu yolla 127 tür cihazı çalıştırma imkânı vardır, ek aparatlarla tek

bağlantı noktasına birden fazla cihaz bağlanabilir. USB basit bir dö

rt telli bağlantıdır. Veri

kodlamasına NRZI (Non

-

return to Zero Inverted) denir. Modern anakartlarda en az 4 USB portu

bulunmaktadır.

USB Hızları



USB 1.0 ve 1.1: Hız 12 Mbit/sn (1.5 MByte/sn) (fullspeed)



USB 2.0: Hız 480 Mbit/sn (60 MByte/sn) (highspeed)



USB 3.0: Hız 4,80 Gbit/sn (600 MByte/sn) (superspeed)



USB 3.1: Hız 10 Gbit/sn (1,22 GByte/sn) (superspeed 10 Gbps)

USB 2.0, 480 Mbps bant genişliği sunabilen USB sürümüdür. USB 1.1 de sunulabilen bant

genişliği 12 Mbps ile sınırlıdır. USB 2.0, USB 1.1'in 4

0 katı kadar bant genişliği ile yüksek hız

sağlar. Her iki sürümde de kablo yapısı ve bağlantı uçları aynıdır. USB 2.0, USB 1.1 ile

uyumludur. Yüksek bağlantı hızı gerektiren harici CD/DVD yazıcı gibi cihazlar USB 2.0 standartını

kullanırlar. USB 3.0 yapıl

an testler sonucunda en fazla 1320/sn Mbit ile sınırlı kaldı, beklenilen

yükseklikte bir hıza ulaşılamadı. 2001 yılında benzer bir durum USB 2.0 için yaşandı. En yüksek

hız olarak 250 Mbit hıza ulaşıldı ancak çalışmalar ilerledikçe USB 2.0 480 Mbit/sn hıza

ulaştı ve

kullanılmaya başlandı. Günümüzdeki en son teknoloji USB 3.0'dır. USB 3.0 beklendiği gibi 5 Gbit

hıza çıkmıştır. Günümüzde yeni üretilen anakartların neredeyse hepsinde USB 3.0 desteği vardır.

Intel ve AMD'nin yeni yonga setlerinin yaygınlaşmasıy

la USB 3.0 gittikçe yüksek hız gerektiren

harici sabit diskler ve flash belleklerde kullanılmaya başlanmıştır. Harici sabit disklerin çoğunluğu

USB 3.0 desteğine sahip olmasına rağmen flash belleklerde henüz USB 3.0 desteği çok azdır ve

pahalıdır.




