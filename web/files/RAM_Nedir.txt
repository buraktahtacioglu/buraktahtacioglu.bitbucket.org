RAM Nedir?

Rastgele erişimli hafıza

(İngilizce:

Random Access Memory

)

mikroişlemci

li sistemlerde

kullanılan bir tür

veri deposu

dur. Buna karşın diğer hafıza aygıtları (

manyetik kasetler

,

disk

ler)

saklama ortamındaki verilere önceden belirlenen bir sırada

ulaşabilmektedir, çünkü mekanik

tasarımları ancak buna izin vermektedir. Bir

RAM yongası

nda herhangi farklı iki veriye ulaşmak

için aşağı yukarı aynı süre harcanmaktadır. Buna karşılık disk ve benzerleri okunan verinin başı

bulunan noktaya yakınsa az zaman

, uzaksa çok zaman harcamakta ve baş konumu sürekli yer

değiştirmektedir.

RAM

,

genellikle bilgisayardaki ana hafıza ya da birincil

depo

;

yükleme

,

gösterme

,

uygulamaları yönlendirme

ve

veri için çalışma alanı

olarak

düşünülür. Bu tip RAM genelde tümleşik dev

re biçimindedir. Yaygın olarak hafıza çubuğu veya

RAM çubuğu isimleriyle anılır çünkü devre kartı üzerine, küçük devreler halinde, plastik paketleme

yardımıyla birkaç sakız paketi boyutundadır. Çoğu kişisel bilgisayarda RAM eklemek veya

değiştirmek için yu

va bulunur. Çoğu

RAM hem yazılıp hem okunabilir

. Bu yüzden RAM sık sık

"okunan

-

yazılan hafıza" ismiyle yer değiştirmiştir. Bu bağlamda

RAM

,

ROM

'un tersi, daha doğrusu

sıralı ulaşılabilir hafızanın tersi olarak kabul edilir.

RAM Çeşitleri

Yazılabilir RAM'in

modern çeşitleri, bilgileri genellikle ya disket içinde (

SRAM

[statik RAM] gibi) ya

da bir kondansatör içinde (

DRAM

[dinamik RAM],

EPROM

,

EEPROM

,

usb

gibi)depo eder. Bazı

çeşitleri, rastgele hataları ortaya çıkarmak ve doğrulamak için

eşlik biti

veya

hata düzeltme

kodları

kullanır. RAM'in salt okunur tipi olan

ROM

,

RAM

yerine kalıcı

-

devre dışı

seçilmiş

transistör

sağlamak için metal kalıp kullanır.

SIMM

ve

DIMM

bellek modülleri özel

husustur. Disk ve manyetik bantlar gibi bilgisayar depolama diğer forml

arı, kalıcı depolama olarak

kullanılmıştır. Birçok yeni ürünün yerine verileri korumak için

flash bellek

itimat değilken

PDA

ya

da

küçük müzik çalar

lar gibi kullanmak içinde. çok sağlam bilgisayarlar ve netbook gibi bazı kişisel

bilgisayarlar, aynı zamanda

flash sürücüler ile

manyetik disk

lerin yerini almıştır.

Flash bellek

ile

sadece NOR tipi ve doğrudan kod çalıştırılmasına, gerçek rastgele erişim yeteneğine sahip ve bu

nedenle sık sık ROM yerine kullanılır; daha düşük maliyetle NAND tipi yaygın hafıza ka

rtları

ve

solid

-

state sürücü

ler toplu depolama için kullanılır.

Genel Özellikler

RAM

,

"Random Access Memory"

(Rasgele Erişimli Bellek) kelimelerinin baş harflerinden oluşan

bir kısaltmadır.

RAM bilgilerin geçici olarak depolandığı bir hafıza türüdür

.

Rast

gele erişimli hafıza

(İngilizce:

Random Access Memory

)

mikroişlemci

li sistemlerde

kullanılan bir tür

veri deposu

dur. Buna karşın diğer hafıza aygıtları (

manyetik kasetler

,

disk

ler)

saklama ortamındaki verilere önceden belirlenen bir sırada

ulaşabilmektedir, çünkü mekanik

tasarımları ancak buna izin vermektedir. Bir

RAM yongası

nda herhangi farklı iki veriye ulaşmak

için aşağı yukarı aynı süre harcanmaktadır. Buna karşılık disk ve benzerleri okunan verinin başı

bulunan noktaya yakınsa az zaman

, uzaksa çok zaman harcamakta ve baş konumu sürekli yer

değiştirmektedir.

RAM

,

genellikle bilgisayardaki ana hafıza ya da birincil

depo

;

yükleme

,

gösterme

,

uygulamaları yönlendirme

ve

veri için çalışma alanı

olarak

düşünülür. Bu tip RAM genelde tümleşik dev

re biçimindedir. Yaygın olarak hafıza çubuğu veya

RAM çubuğu isimleriyle anılır çünkü devre kartı üzerine, küçük devreler halinde, plastik paketleme

yardımıyla birkaç sakız paketi boyutundadır. Çoğu kişisel bilgisayarda RAM eklemek veya

değiştirmek için yu

va bulunur. Çoğu

RAM hem yazılıp hem okunabilir

. Bu yüzden RAM sık sık

"okunan

-

yazılan hafıza" ismiyle yer değiştirmiştir. Bu bağlamda

RAM

,

ROM

'un tersi, daha doğrusu

sıralı ulaşılabilir hafızanın tersi olarak kabul edilir.

RAM Çeşitleri




Yazılabilir RAM'in

modern çeşitleri, bilgileri genellikle ya disket içinde (

SRAM

[statik RAM] gibi) ya

da bir kondansatör içinde (

DRAM

[dinamik RAM],

EPROM

,

EEPROM

,

usb

gibi)depo eder. Bazı

çeşitleri, rastgele hataları ortaya çıkarmak ve doğrulamak için

eşlik biti

veya

hata

düzeltme

kodları

kullanır. RAM'in salt okunur tipi olan

ROM

,

RAM

yerine kalıcı

-

devre dışı

seçilmiş

transistör

sağlamak için metal kalıp kullanır.

SIMM

ve

DIMM

bellek modülleri

özel

husustur. Disk ve manyetik bantlar gibi bilgisayar depolama diğer formları, kalıcı depolama olarak

kullanılmıştır. Birçok yeni ürünün yerine verileri korumak için

flash bellek

itimat değilken

PDA

ya

da

küçük müzik çalar

lar gibi kullanmak içinde. çok

sağlam bilgisayarlar ve netbook gibi bazı kişisel

bilgisayarlar, aynı zamanda flash sürücüler ile

manyetik disk

lerin yerini almıştır.

Flash bellek

ile

sadece NOR tipi ve doğrudan kod çalıştırılmasına, gerçek rastgele erişim yeteneğine sahip ve bu

nedenle s

ık sık ROM yerine kullanılır; daha düşük maliyetle NAND tipi yaygın hafıza kartları

ve

solid

-

state sürücü

ler toplu depolama için kullanılır.

Genel Özellikler

RAM

,

"Random Access Memory"

(Rasgele Erişimli Bellek) kelimelerinin baş harflerinden oluşan

bir k

ısaltmadır.

RAM bilgilerin geçici olarak depolandığı bir hafıza türüdür

. Bilgisayarlar

genellikle o an üzerinde çalıştıkları programlar ve işlemlerle ilgili bilgileri RAM denen bu hafıza

parçasında tutarlar. RAM ve sabit sürücü temel olarak aynı bilgileri

saklarlar, ancak işlemcinin

RAM'deki bilgilere erişme ve onları işleme hızı, sabit sürücüdeki bilgilere erişme ve onları işleme

hızından çok daha büyüktür. Bilgisayarlar işlem yaparken program kodları ve veri tutmak için RAM

kullanırlar. RAM'in karakterini

tanımlayan özelliği bütün hafıza noktaları neredeyse aynı hızda

erişilebilir olmasıdır. Diğer teknolojilerin çoğu belirli bir bit veya byte okuduklarından gecikmelere

sebebiyet verir. Birçok RAM türü uçucudur. Bunun anlamı

disk

ve

kaset

gibi hafıza depola

ma

aygıtlarından farklı olarak

bilgisayar kapatıldığında içerdiği veriyi kaybetmesidir

. Yeni nesil

RAM'ler, bir bitlik veriyi dinamik RAM'lerdeki gibi

kapasitör

de akım olarak ya da statik RAM'lerdeki

gibi bir

flip

-

flop

'ta durum olarak saklar. Bunun anlamı

d

isk

ve

kaset

gibi hafıza depolama

aygıtlarından farklı olarak

bilgisayar kapatıldığında içerdiği veriyi kaybetmesidir

. Yeni nesil

RAM'ler, bir bitlik veriyi dinamik RAM'lerdeki gibi

kapasitör

de akım olarak ya da statik RAM'lerdeki

gibi bir

flip

-

flop

'ta

durum olarak saklar. Yazılımlar RAM'leri ayırarak bir kısmının daha hızlı sabit

disk gibi çalışmasını sağlayabilir. Buna ‘RAM Disk’ denir. Kullanılan hafıza kaydedilmemiş ise, RAM

disk bilgisayar kapandığında veriyi kaybeder. Ama kaydedilmemiş hafıza ayrı

bir güç kaynağına

sahip ise

-

pil gibi

-

veriyi kaybetmez.

RAM Tarihçesi

İlk zamanlar yaygın yazılabilir RAM, 1949

-

1952 yılları arasında geliştirildi. Manyetik çekirdek bellek

olarak birçok bilgisayarda kullanıldı. Daha sonra 1960'ların sonu ve 1970'lerin

başında statik ve

dinamik entegre devreler geliştirildi. İlk ana hafıza sistemleri, bugünkü RAM gibi, vakum tüplerinden

oluşturulmuştur, ama sıklıkla başarısız olmuşlardır. Çekirdek hafıza, küçük ferrit elektromanyetik

çekirdeklere tellerle bağlanan, eşit

ulaşım zamanlamasına pek sahip değildi. Çekirdek terimi bazı

programcılar tarafından RAM'lerin bilgisayarın ana hafızası anlamında kullanılmaktadır. Tüp ve

çekirdek hafızanın temel konsepti günümüz RAM'lerindeki tümleşik devrelerde kullanılır. Alternatif

b

irincil depolama mekanizmaları genellikle tek biçimli olmayan hafıza erişim gecikmelerini içerir.

Gecikme satır hafızası bitleri tutmak için cıva dolu tüplerde ses dalga dürtü serisi

kullanılmıştır.

Tambur hafıza

günümüz sabit diskleri gibi sürekli yuvarla

k manyetik bantlarda veriyi

saklamıştır.

Tambur hafıza

günümüz sabit diskleri gibi sürekli yuvarlak manyetik bantlarda veriyi

saklamıştır.

DRAM (Dinamik Rastgele Erişimli Bellek)

Ekonomik nedenlerden dolayı, kişisel bilgisayarlarda, iş istasyonlarında, ko

ntrol edilmeyen oyun

konsollarında (Playstation, Xbox gibi) geniş hafızalar dinamik RAM'lerden oluşur. Bilgisayarın diğer

kısımları zula hafıza (önbellek) ve diğer disklerdeki veri tamponları statik RAM kullanır. Dinamik

rastgele erişimli hafıza (DRAM) tüm

leşik devrelerin plastik ambalaja metal iğnecikler ile bağlanıp,




sinyaller ile kontrol edilecek biçimde üretilir. Dinamik denmesinin nedeni enerjiyi saklamak için

saniyede yüz defaya yakın içinde bulunan kondansatörlerin yüklenmesi gerekir. Günümüzde bu

DR

AM'ler kolay kullanım için rahat takılacak modüllerden oluşur.

SRAM (Statik Rastgele Erişimli Bellek)

Her hücre için altı adete varan

transistör

kullanılır.

Her hücre için altı adete varan

transistör

kullanılır. Bu tip RAM'lerde bilgiler yüklendikten sonr

a

sabit kalır. Sürekli enerji tazelemesi gerekmemektedir. SRAM (statik RAM), DRAM'den daha hızlı

ve daha güvenilir olan ama onun kadar yaygın değildir. SRAM'lerin üretim maliyetlerinin

DRAM'lerinkine oranla çok daha yüksektir.

Son Gelişmeler

Kapatıldığınd

a ise uçucu olmayan yani verileri koruyabilecek birkaç RAM türü geliştirilmektedir. Bu

gelişmelerde kullanılan teknolojiler ise

karbon nanotüp

ler ve

manyetik tünel

etkisi kullanan

yaklaşımlar içerir. 2006 dan bu yana, kapasiteleri 64 GB olan ve performansl

arı alışılagelmiş

disklerin çok üstünde olan

Katı Hal Sürücüler

(

Solid

-

state Drives

) (

flash bellek

tabanlı) mevcut

duruma gelmiştir. Bu gelişmelerde kullanılan teknolojiler ise

karbon nanotüp

ler ve

manyetik

tünel

etkisi kullanan yaklaşımlar içerir. 2006 dan

bu yana, kapasiteleri 64 GB olan ve

performansları alışılagelmiş disklerin çok üstünde olan

Katı Hal Sürücüler

(

Solid

-

state Drives

)

(

flash bellek

tabanlı) mevcut duruma gelmiştir.




