Klavye Nedir ?

Tuştakımı ya da klavye bilgisayarın en önemli giriş ögesidir. Üzerinde harfler, rakamlar, işaretler ve

bazı işlevleri bulunan tuşlar vardır. Türkçeye özel iki tür tuş düzenlemesi vardır: F klavye ve Q

klavye diye ikiye ayrılır.

Çalışması

Tuş takımında her tuşa iki kod atanmıştır (Hex kodu). Tuşa basınca oluştur kodu tetiklenir. Tuş

bırakıldığı zamanda bitir kodu tetiklenir. Karakter atamaları kod sayfalarında saklıdır. Bunlar her

tuş koduna belirli bir karakter karşılığı düşürürler. KEYB k

omutu basit anlamda klavyeden

gönderilen her kod için uygun bir karakter atamakta kullanılan bir tabloyu yükler. Bu işlemi çok hızlı

zaman içinde gerçekleştirdiğinden bize kolay bir iş gibi gelir. Ancak yukarıda anlatıldığı gibi karışık

bir işlemi vardır.

Türleri

Q Klavye ve F Klavye (Türkçe Daktilo Klavyesi) olmak üzere iki şekilde sınıflandırılabilir.



Q Türkçe tuştakımı 91



F Türkçe tuştakımı 91

Tuştakımı üzerinde numaralar, Kilitler (Caps Lock: Bir kez basıldığında sürekli büyük harf yazar.

İkinci kez bas

ıldığında sürekli küçük harf yazar, Num Lock: Klavyenin sağında bulunan Nümerik

alanın aktif veya pasif olmasını sağlar, Scroll Lock), Özel Tuşlar (Alt, Shift, Control, Alt Gr). Dizgeye

(sistem) komut verme, veri girme bakımından tuştakımı sistemin en önem

li giriş aygıtıdır. 101/102

tuşlu klasik ve 104 tuşlu win tuştakımı standarttır. Fişleri DIN veya USB bağlantılı olan tuştakımı

kablosunun öbür ucu klavye kasası içinde yahut bir fiş şeklinde olabilir. Kablolu veya kablosuz

kumandalı klavye setleri bulunma

ktadır. Standart düzen QWERTY'dir.

Bölümleri

Tuştakımını 5 bölümde inceleyebiliriz.

1.

Harf, rakam, işaretler bölümü,

2.

Sayı bölümü,en üstte Numlock, /, *,

-

, + ve Enter, onun yanında Del,

3.

Bunların arasında imleç bölümü, onun üzerinde Insert, Home, Pageup, Page

down, End,

Delete,

4.

Harf bölümü üzerinde dörtlü gruplar halinde işlev tuşları bölümü, F1

-

den F12'ye kadar ve

onun yanında Print, Scroll Lock, Kesme tuşları,

5.

Yeni klavyelerde en üstte tarama butonları, eposta, media, calculator tuşları bölümü.

Harf bölümünde

solda Tab, Caps Lock, sol Shift, sol Ctrl ve sağda Backspace, Enter, sağ Shift,

sağ Ctrl, onun yanında Windows'un iki tuşu ve solda Boşluk tuşu bulunmaktadır. Sol üstte tek

başına Esc tuşu vardır.

Kullanımı

Tuştakımı hatasında önce kablo denetlenmeli, boz

uksa değiştirilmelidir. Tuşların biri veya birkaçı

çalışmıyorsa çıkarılıp saf suyla temizlenmelidir. Klavyeler artık çok ucuzladığı için gövdedeki

bozukluklarda komple değiştirilmektedir. Harf bölümünde F tuştakımında A ve K üzerinde, Q

tuştakımında F ve J

üzerinde ve bütün klavyelerde sayı bölümünün 5'i üzerinde başlama kabartıları

vardır. Standart tuştakımı dışında her dilin fiziksel tuştakımı bulunur.




Acil Durumda Ekran Klavyesine Erişim

Windows XP kullanıyorsanız. Başlat

-

Tüm Programlar(Programlar)

-

Donatılar

-

Erişilebilirlik

-

Ekran Klavyesi Yörüngesini izliyerek "Ekran Klavyesi"ne ulaşabilirsiniz Bundan sonra yapacağınız

şey, farenin imlecini parmaklarınız gibi kullanmaktır. Ekran Kl

avyesi programı, Windows'un daha

ileri sürümünde de bulunur.




