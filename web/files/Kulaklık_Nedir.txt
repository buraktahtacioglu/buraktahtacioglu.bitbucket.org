Kulaklık Nedir ?

Kulaklık, 2 küçük hoparlörün kullanıcının kulağına yakın yerde durmasını sağlayan bir çevre

birimidir. Bir kulak üstü kulaklık. Genellikle kullanım amaçları, kullanan kişi dışında başka kişileri

rahatsız etmemek, dışarıdaki ortam

gürültüsünü önlemek veya kulakları korumaktır. Kulaklıklar 2

ana başlıkta toplanabilir.

Kafa Üstü Kulaklık

Kafa Üstü kulaklıklar (İng. Headphones), genellikle bilgisayarlar için kullanılan kulaklıklardır. Çok

yer kapladığından ve taşınabilir olmadığından d

olayı mobil cihazlarda pek tercih edilmezler.

Kulaklık firmaları bu sorunu çözmek için kulaklıklara katlanabilme

-

dönebilme özellikleri

eklemektedir. Üzerine mikrofon entegre edilmiş modelleri de bulunmaktadır. Kullanım yelpazesi

geniştir. Bilgisayar kullan

ıcıları, oyuncular, radyocular, disk jokeyler vb. bu kulaklık türünü tercih

etmektedir. Ses kalitesi bakımından kulak içi kulaklıklardan daha kaliteli ses üretirler. İki kulaklık

süngerinin içinde 2 hoparlör olabileceği gibi ek olarak bas hoparlörleri ve t

iz hoparlörleri de

eklenebilir. Beats, SteelSeries, Logitech ve Sony, kafa üstü kulaklık üreten büyük firmalardan

bazılarıdır.

Kulak İçi Kulaklık

Kulak İçi kulaklıklar (İng. Earbuds veya Earphones), kafa üstü kulaklıkların gündelik kullanıma

uyarlanmış mod

elidir. Dış veya orta kulak içine takılabilen modelleri vardır. Orta kulak içine takılan

modellerin baş kısmında şapkamsı, silikon bir yapı mevcuttur. Günümüzde bu kulaklıkların içine de

birden çok hoparlör yerleştirilebilmekte, MP3 entegre edilebilmektedi

r. Bu kulaklık türü mobil

cihazların yanında tercih edilmektedir. Bluetooth kulaklıklar da bu kulaklık türüne dahildir. Beats,

Sony, Samsung ve Apple, kulak içi kulaklık üreten büyük firmalardan bazılarıdır. Ayrıca bu

kulaklıklar, üretici tarafından üretil

en cihazların yanında da verilir.

Diğer Kullanım Alanları

Kulaklık, dışarıdaki yüksek gürültüyü engellemek, kulağa herhangi bir maddenin kaçmasını

engellemek, işitme kaybını gidermek ve benzeri amaçlarla da kullanılmaktadır. Bunlara kulak tıkacı

da denilir

.




